# Using Program R for Spatial Models
## Introduction
This project aims to compare the effectiveness of using spatial models to predict the presence and absence of Uinta Basin cacti. Otherwise known as binary classification, presence and absence will be predicted by two popular machine learning algorithms, logistic regression, and random forest. Raster and vector spatial analyses performed using R Spatial to attribute a point-based dataset. 

## Methods

> "Machine learning is the process of mathematical algorithms learning patterns or trends on previously recorded data observations then makes a prediction or  >classification" (Kirasich 2018).
    
Investigating this topic includes comparing outputs of presence-absence distribution models, specifically logistic regression and random forest, from samples from the same cacti dataset, fitting the results back to a landscape alongside a validation dataset not used to train the models. Area Under the Curve (AUC) and Receiver Operator Characteristics (ROC) will also be considered (Alessandro 2015). The area under the curve is the space between the actual prediction rate and the false prediction rate, and is known as the ROC curve. A rate of 1 is perfect, and 0 is abysmal. A decent model needs to have generally over .7 for eligibility, but .9 would be a good target, while the model would be a perfect 1.0 ideally.
    
This report uses a Jupyter Notebook to document and replicate the processes from reading the shapefiles to displaying the results and can be found here. The data and the rest of the repository is available here. It is also helpful to use Anaconda and Anaconda Navigator to manage a virtual environment and handle any package conflicts between this project and others. It is advisable to use RStudio, or some other IDE, for editing and modifying the notebook as they provide helpful IntelliSense providing contextual information while coding.
    
After setting up the environment, install the appropriate packages listed in the first cell of the included Jupyter notebook titled UsingProgramRForSpatialModels_V2.ipynb. Installing packages can be accomplished in an IDE or by using Anaconda Navigator. Packages include sf, or simple features useful for plotting spatial vector data, raster, used for plotting and spatial functions such as intersecting, Metrics, the package containing the AUC functions, ROCR, for calculating the curve sensitivity curve of the models for comparison, spatialEco, for calculating various indicators to be used in the model, tidyverse, used for ggplot, randomforest, used for the random forest algorithm, and rfUtilities, for testing the model for collinearity. 
    
The spatialEco package helped generate geomorphometrics for the model to predict. The functions selected are slope and aspect using the terrain function, which generates values based on a raster, hli function, which calculates the McCune & Keon (2002) Heat Load Index that indicates the amount of temperature variation in the slope and aspect of the surface. The tpi function was selected as well, which calculates position using mean deviation, or in more words: "TPI measures the relative topographic position of the central point as the difference between the elevation at this point and the mean elevation within a predetermined neighbourhood" (Reu et al., 2012). Lastly the trasp function was selected, which generates a solar radiation index using slope and aspect to give a value from 0 to 1 based on the angle away from or towards the sun (Roberts and Cooper 1989). 	

## Data

The data set is obtained from Dr. Shannon Albeke from his Advanced Spatial Statistics class taught in 2014. 

## Hypothesis

The random forest algorithm produces a more accurate prediction of where Uinta Basin cacti are present and absent than what the logistic regression model produces. This hypothesis stems from the fact that random forest not making an underlying assumption of a linear relationship as logistic models do(Kirasich 2018). In either case, a reproducible workflow will be generated and documented for others to follow along if they wish.

## Discussion
    
The results of the random forest model were expected to be better in comparison to the logistic regression based on the different assumptions these two models make, as stated in the hypothesis. However, it was surprising how accurate the random forest model had predicted presence-absence and assumed that something was not working correctly. After many hours of adjusting parameters and ending up with the same result, the perfect prediction rate was accepted. Reducing the sampling size or increasing it did not affect the random forest model. However, it improved upon the logistic regression model a bit by the end of selecting and changing variables.
	
In conclusion, this was an excellent learning experience formatting the data to be in a proper format for two different types of models and isolating an environment using Anaconda so that others can directly replicate the conditions without exorbitant effort. Using the Jupyter notebook as an interface and display tool was also valuable. Ideally, the researcher could download and follow the instructions and reproduce this for themselves.
 
## Bibliography

Alessandro Trigila, Carla Iadanza, Carlo Esposito, Gabriele Scarascia-Mugnozza, Comparison of Logistic Regression and Random Forests techniques for shallow landslide susceptibility assessment in Giampilieri (NE Sicily, Italy), Geomorphology, Volume 249, 2015, Pages 119-136, ISSN 0169-555X, https://doi.org/10.1016/j.geomorph.2015.06.001.

Bivand, Roger. "'The Problem of Spatial Autocorrelation:' Forty Years On." R-Spatial.github.io, 18 Aug. 2017, r-spatial.github.io/spdep/articles/CO69.html. Accessed 28 Apr. 2021.

Desheng Liu, Maggi Kelly, Peng Gong, Qinghua Guo, Characterizing spatial–temporal tree mortality patterns associated with a new forest disease, Forest Ecology and Management, Volume 253, Issues 1–3, 2007, Pages 220-231, ISSN 0378-1127, https://doi.org/10.1016/j.foreco.2007.07.020.

Kirasich, Kaitlin; Smith, Trace; and Sadler, Bivin (2018) "Random Forest vs Logistic Regression: Binary Classification for Heterogeneous Datasets," SMU Data Science Review: Vol. 1 : No. 3 , Article 9. Available at: https://scholar.smu.edu/datasciencereview/vol1/iss3/9

McCune, B., and D. Keon (2002) Equations for potential annual direct incident radiation and heat load index. Journal of Vegetation Science. 13:603-606.

Reu, J. D., Bourgeois, J., Bats, M., Zwertvaegher, A., Gelorini, V., Smedt, P. D., … Crombé, P. (2012, December 22). Application of the topographic position index to heterogeneous landscapes. Geomorphology. https://www.sciencedirect.com/science/article/pii/S0169555X12005739.

Wiegand, T., & Moloney, K. (2004). Rings, Circles, and Null-Models for Point Pattern Analysis in Ecology. Oikos, 104(2), 209-229. Retrieved April 28, 2021, from http://www.jstor.org/stable/3547954

