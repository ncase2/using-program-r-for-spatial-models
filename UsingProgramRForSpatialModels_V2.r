require(sf)
require(rgdal)
require(raster)
require(Metrics)
require(ROCR)
require(spatialEco)
require(tidyverse)
require(randomForest)
require(rfUtilities)

setwd("C:/Users/ncase2/Documents/GIST5220/Final/notebooks")
cactus<- st_read(dsn = ".", layer = "Cactus")
strm<- st_read(dsn = ".", layer = "NHD_Streams") %>% 
  st_zm()
stdy<- st_read(dsn = ".", layer = "StudyArea")

plot(strm$geometry, col = "blue")
plot(stdy$geometry, add = TRUE)
plot(cactus$geometry, add = TRUE, col="red")

View(cactus)
View(strm)
View(stdy)

str(stdy)
strm[5,]


#fix shape length
istrm<- st_intersection(strm, stdy) %>% 
  mutate(Shape_Leng = st_length(geometry))


plot(istrm$geometry, col = "blue")
plot(stdy$geometry, add = TRUE)
plot(cactus$geometry, add = TRUE, col = "red")

# dissolve, and strip out all attributes to just geom
istrm<- st_union(istrm)

pbuff<- st_buffer(cactus, dist = 1000) 

plot(pbuff$geometry, add=TRUE)

pdis<- st_union(pbuff) #dissolve the buffered points first; rgeos

pdiff<- st_difference(stdy, pdis) #get the geomtric difference; rgeos

plot(pdiff$geometry, col="red")

#buffer study area by km and get extent to create new polygon
rstdy<- stdy %>% 
  st_buffer(dist = 1000) %>% 
  st_bbox() %>% 
  st_as_sfc()
plot(rstdy, add = TRUE)

#make random points and get geometry
rp<- st_sample(stdy, size = nrow(cactus), type = "random") %>% 
  st_as_sf(data.frame(PointID = 1:nrow(cactus), Present = 0)) %>% 
  rename(geometry = x)
cactus<- rbind(cactus, rp)

#check results
st_geometry(cactus) <- "geometry"
ggplot() + 
  geom_sf(data = rstdy) + 
  geom_sf(data = cactus, aes(group=as.factor(Present), color = as.factor(Present)))

#Raster

dem<- raster("DEM_30m.img")

plot(dem)
plot(stdy$geometry, add=TRUE, col="red")

cdem<- crop(dem, as(rstdy,"Spatial"))

plot(cdem)
plot(rstdy, add=TRUE)
plot(cactus$geometry, add=TRUE, col=ifelse(cactus$Present==1, "black", "red"))

# extent, projection, resolution, same origin
# 4 tenets of raster analyses ^

slp<- terrain(cdem, opt = "slope", unit = "degrees", progress = "text")
asp<- terrain(cdem, opt = "aspect", unit = "degrees", progress = "text")
# do spatial eco stuff
# aka geomorphometrics

hli<- hli(cdem, force.hemisphere = "northern")
tpi3<- tpi(cdem, scale = 3, win = "rectangle")
# same as a ten by ten rectangular window but leaving off the corners
tpi300<- tpi(cdem, scale = 300, win = "circle")
# Solar-radiation Aspect Index
trasp<- trasp(cdem)


# add outputs to the stack
rs<- stack(cdem, slp, hli, tpi3, tpi300, trasp)
# rename covariates 
names(rs)<- c("DEM_30m", "Slope", "HLI", "TPI3", "TPI300", "TRASP")
e<- data.frame(raster::extract(rs, as(cactus,"Spatial")))

# combine with input
cactus<- cactus %>% 
  bind_cols(e)

#plot the relationship between elev and slope
plot(cactus$DEM_30m, cactus$Slope)

# inspect output and check for nulls and weird values
# 
summary(cactus)

# look for colinearity between parameters
# abs value of .7 means params cant be in the same model, too colinear
# test for model assumptions
cor(cactus[,3:7] %>% 
      st_drop_geometry(), use = "complete.obs")

# TPI300 and DEM too highly correlated. Drop TPI 300
 
#model formula
modLR <- glm(Present~DEM_30m + Slope + HLI + TRASP, data=cactus %>% 
               st_drop_geometry(), family=binomial(link="logit"))

# get the coefficients
# error around estimate crosses zero because p > .05
# more parsimonious = less complex = better 
# use AIC to determine, lower is better

summary(modLR)

# increasing in elevation, less cactus
# increasing slope, less cactus

#calculate the AUC....Our model is particularly good, but this is a demo, right?!!
# extra parens mean print to screen and save to an object
( lrAUC<- auc(cactus$Present, modLR$fitted.values) )
# .7 generally threshold, .9 target, 1 ideally.
#get the ROC plot

pred <- prediction(fitted(modLR), cactus$Present)
perf <- performance(pred, measure="tpr", x.measure="fpr")
plot(perf, col=rainbow(10))
abline(coef=c(0,1))

#predict back to the landscape
pred<- predict(rs, modLR, type='response', progress="text")
plot(pred)
plot(cactus$geometry, add=TRUE, col=ifelse(cactus$Present==1, "black", "red"))

# choose some threshold: good for cacti or not
# good probabilities for where cactus are found
# create data frame of inputs vs fitted values
foundCactus<-data.frame(obs = cactus$Present, fitted = modLR$fitted.values) %>% 
  group_by(obs) %>% 
  summarise(Min = min(fitted), 
            Max = max(fitted))

# .3 selected as threshold
rcls<-data.frame(from = c(0, .3), to = c(.3, 1), becomes = c(0, 1))

predClass<- reclassify(pred, rcl = as.matrix(rcls))
freq(predClass)

# areas that might want to be protected according to our model and threshold.
plot(predClass)

# random forest

# test for Multi-collinearity
# null is desirable, else take out the params that show and remove
mCol<- multi.collinear(cactus %>% 
                         st_drop_geometry() %>% 
                         dplyr::select(DEM_30m:TRASP))

# model selection
rf.modelSel(xdata = cactus %>% 
              st_drop_geometry() %>% 
              dplyr::select(DEM_30m:TRASP), ydata = as.factor(cactus$Present))
# use "Selected variables:" output 
# odd numbers
modRf<- randomForest(x = cactus %>% 
                       st_drop_geometry() %>% 
                       dplyr::select(DEM_30m:TRASP), y = as.factor(cactus$Present), ntree = 201, importance = TRUE)
modRf

# 19% kinda high error

# hacky code
# internal pred response
pred2<- predict(modRf, cactus %>% 
                  st_drop_geometry() %>% 
                  dplyr::select(DEM_30m:TRASP), type ="response")
View(pred2)
# internal prob response
obsProb<- as.data.frame(predict(modRf, cactus %>% 
                    st_drop_geometry() %>% 
                    dplyr::select(DEM_30m:TRASP), type = "prob"))
head(obsProb)
# make data frame with pred and prob
mObsPred<- data.frame(Observed = as.numeric(as.character(cactus$Present)),
                            PRED = as.numeric(as.character(pred2)),
                            Prob1 = obsProb[,2],
                            Prob0 = obsProb[,1])
head(mObsPred)
# count correct preds
mop<-(mObsPred$Observed == mObsPred$PRED)
table(mop)
# validation rate
mpcc<- (length(mop[mop=="TRUE"])/length(mop))*100
mpcc
# internal auc
intAUC<- auc(cactus$Present, obsProb[,2])
intAUC

prdctnRf <- prediction(mObsPred$PRED, mObsPred$Observed)

perfRf <- performance(prdctnRf, measure="tpr", x.measure="fpr")
plot(perfRf, col=rainbow(10))
abline(coef=c(0,1))

# predict back to the landscape
predRf<- predict(rs, modRf, type='prob', progress="text")
plot(predRf)
plot(cactus$geometry, add=TRUE, col=ifelse(cactus$Present==1, "black", "red"))

# choose some threshold: good for cacti or not
# good probabilities for where cactus are found
# create data frame of inputs vs fitted values
foundCactus<-data.frame(obs = cactus$Present, fitted = mObsPred$Prob1) %>% 
  group_by(obs) %>% 
  summarise(Min = min(fitted), 
            Max = max(fitted))
foundCactus
# .3 selected as threshold
rclsRf<-data.frame(from = c(0, .41), to = c(.41, 1), becomes = c(0, 1))

predClassRf<- reclassify(pred, rcl = as.matrix(rcls))
freq(predClassRf)

# areas that might want to be protected according to our model and threshold.
plot(predClassRf)

# Add predicted logistic reg to random forest totals and compare total area
# using frequency functions calculate total areas, plot it